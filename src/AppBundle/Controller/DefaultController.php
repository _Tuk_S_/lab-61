<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Purchases;
use AppBundle\Entity\User;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{

    /**
     * @Route("/", name="homepage")
     */
    public function indexAction()
    {
        $session = $this->get('session');
        $session_id = $session->getId();

        if (!$session->has($session_id)) {
            $session->set($session_id, 0);
        }

        $restaurants = $this->getDoctrine()
            ->getRepository('AppBundle:Restaurants')
            ->findAll();

        $dishes_2017 = $this
            ->getDoctrine()
            ->getRepository('AppBundle:Purchases')
            ->getPurchasesByMinDishesCount('2016-31-12');


        return $this->render('default/index.html.twig', [
            'dishes_2017' => $dishes_2017,
            'restaurants' => $restaurants,
        ]);
    }


    /**
     * @Route("/purchases")
     * @Method({"GET", "POST"})
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */

    public function purchasesAction(Request $request)
    {
        $to_basket = [];
        foreach ($request as $value) {
            foreach ($value as $key => $val) {
                $to_basket[$key] = $val;
            }
        }

        $to_basket1 = [];

        $i = 1;
        foreach ($to_basket as $key => $value) {
            $name = 'dish_name' . $i;
            if ($key == $name) {
                $i++;
                $to_basket1[] = $value;
                $purchases = new Purchases();
                $em = $this->getDoctrine()->getManager();
                $purchases->setUser($this->getUser());
                $purchases->setPublishDate(new \DateTime());
                $purchases->setDishes($this->getDoctrine()
                    ->getRepository('AppBundle:Dishes')
                    ->find($value));
                $em->persist($purchases);
                $em->flush();
            }
        }

        return $this->redirectToRoute('homepage');
    }


    /**
     * Creates a new Dishes entity.
     * @Route("/dishes/{id}", requirements={"id": "\d+"})
     * @Method({"GET", "POST"})
     * @param $id integer
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */

    public function dishesShowAction(Request $request, int $id)
    {
        $session = $this->get('session');

        $em = $this->getDoctrine()->getManager();
        $dishes = $em->getRepository('AppBundle:Dishes')->findAll();

        $result = [];

        foreach ($dishes as $key => $val) {
            if ($val->getRestaurants()->getId() != $id) {
                continue;
            }
            $result[] = [
                'id' => $val->getId(),
                'name' => $val->getName(),
                'descriptions' => $val->getDescriptions(),
                'price' => $val->getPrice()
            ];
        }

        $restaurants = $this->getDoctrine()->getManager()
            ->getRepository('AppBundle:Restaurants')
            ->findAll();
        $restaurant_id = [];

        foreach ($restaurants as $value) {
            if ($value->getId() != $id) {
                continue;
            }
            $restaurant_id[] = [
                'id' => $value->getId(),
                'descriptions' => $value->getDescriptions(),
                'title' => $value->getTitle()
            ];
        }

        $dishes_array = [];
        foreach ($_SESSION as $key => $value) {
            if ($key == $session->getId()) {
                $dishes_array[] = $value;
                continue;
            }
        }

        return $this->render('default/dishes_show.html.twig', [
            'dishes' => $result,
            'restaurant' => $restaurant_id,
            'dishes_array' => $dishes_array ?: '',
            'id' => $_SESSION,
            'restaurant_id' => $id,
            'user' => $this->getUser(),
        ]);
    }


    /**
     * @Route("/basket/{id}", requirements={"id": "\d+"})
     * @Method({"GET", "POST"})
     * @param $id integer
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function basketAction(Request $request, int $id)
    {

        $user = $this->getUser();

        if ($user == null) {
            return $this->render('FOSUserBundle::layout.html.twig');
        }


        $session = $this->get('session');
        $session_id = $session->getId();

        if (!$session->has($session_id)) {
            $session->set($session_id, 1);
        } else {
            $counter = $session->get($session_id);
            $session->set($session_id, $counter + 1);
        }

        $count = $session->get($session_id);

        $to_basket = [];
        foreach ($request as $value) {
            foreach ($value as $key => $val) {
                $to_basket[$key] = $val;
            }
        }

        $em = $this->getDoctrine()->getManager();
        $dish = $em->getRepository('AppBundle:Dishes')
            ->find($to_basket['to_basket_id']);

        $_SESSION[$session_id][$count] = $dish;

        return $this->redirectToRoute('app_default_dishesshow', ['id' => $id]);
    }
}
