<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
///**
// * Restaurants
// *
// * @ORM\Table(name="restaurants")
// * @ORM\Entity(repositoryClass="AppBundle\Repository\RestaurantsRepository")
// */


/**
 * Restaurants
 *
 * @ORM\Table(name="restaurants")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\RestaurantsRepository")
 * @Vich\Uploadable
 */
class Restaurants
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=127)
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(name="picture", type="text", nullable=true, unique=false)
     */
    private $picture;

    /**
     * NOTE: This is not a mapped field of entity metadata, just a simple property.
     *
     * @Vich\UploadableField(mapping="picture_file", fileNameProperty="picture")
     *
     * @var File
     */
    private $imageFile;

    /**
     * @var string
     *
     * @ORM\Column(name="descriptions", type="text")
     */
    private $descriptions;


    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Dishes", mappedBy="restaurants")
     */
    private $dishes;

    public function __construct()
    {
        $this->dishes = new ArrayCollection();
    }

    public function getDishes()
    {
        return $this->dishes;
    }



    /**
     *
     * @param File|\Symfony\Component\HttpFoundation\File\UploadedFile $image
     *
     * @return Restaurants
     */
    public function setImageFile(File $image = null)
    {
        $this->imageFile = $image;
        return $this;
    }

    /**
     * @return File|null
     */
    public function getImageFile()
    {
        return $this->imageFile;
    }


    public function __toString()
    {
        return $this->title ?: '';
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return Restaurants
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }



    /**
     * Set picture
     *
     * @param string $picture
     *
     * @return Restaurants
     */
    public function setPicture($picture)
    {
        $this->picture = $picture;

        return $this;
    }

    /**
     * Get picture
     *
     * @return string
     */
    public function getPicture()
    {
        return $this->picture;
    }
    /**
     * Set descriptions
     *
     * @param string $descriptions
     *
     * @return Restaurants
     */
    public function setDescriptions($descriptions)
    {
        $this->descriptions = $descriptions;

        return $this;
    }

    /**
     * Get descriptions
     *
     * @return string
     */
    public function getDescriptions()
    {
        return $this->descriptions;
    }

    /**
     * Add dish
     *
     * @param \AppBundle\Entity\Dishes $dish
     *
     * @return Restaurants
     */
    public function addDish(\AppBundle\Entity\Dishes $dish)
    {
        $this->dishes[] = $dish;

        return $this;
    }

    /**
     * Remove dish
     *
     * @param \AppBundle\Entity\Dishes $dish
     */
    public function removeDish(\AppBundle\Entity\Dishes $dish)
    {
        $this->dishes->removeElement($dish);
    }
}
