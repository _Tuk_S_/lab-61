<?php

namespace AppBundle\Repository;

/**
 * PurchasesRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class PurchasesRepository extends \Doctrine\ORM\EntityRepository
{
    public function getPurchasesByMinDishesCount($date)
    {
        return $this
            ->createQueryBuilder('p')
            ->select('p')
//            ->leftJoin('p.dishes', 'd')
//            ->groupBy('d.name')
            ->having('p.publishDate > :date')
            ->setParameter('date', $date)
            ->getQuery()
            ->getResult();
    }
}
