<?php

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\Dishes;
use AppBundle\Entity\Restaurants;
use AppBundle\Entity\User;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;


class LoadArticleData implements FixtureInterface, ContainerAwareInterface
{
    /**
     * @var ContainerInterface
     */
    private $container;

    public function load(ObjectManager $manager)
    {
        $user = new User();
        $user
            ->setUsername('Doe')
            ->setRoles(["ROLE_ADMIN"])
            ->setEmail('original@some.com')
            ->setEnabled(true)
            ->setAddress('Paris')
            ->setPhone('12-12-12')
            ->setName('Jonn');

        $encoder = $this->container->get('security.password_encoder');
        $password = $encoder->encodePassword($user, '123321');
        $user->setPassword($password);

        $manager->persist($user);


        $restaurant1 = new Restaurants();
        $restaurant1
            ->setTitle('Ла Пешериа')
            ->setDescriptions(
                'Ресторан при отеле Exedra славится тем, что здесь кухня не отгорожена от обеденного зала, благодаря чему гости могут наблюдать за тем, как в руках шеф-повара свежайшие продукты превращаются в готовые блюда. La Pescheria предлагает поужинать или пообедать как на террасе, оформленной светло и ярко, так и во внутреннем зале, который декорирован темными оттенками коричневого и украшен камином.'
            );

        $restaurant1->setPicture('r1.jpg');

        $manager->persist($restaurant1);


        $restaurant2 = new Restaurants();
        $restaurant2
            ->setTitle('Дом Мильера')
            ->setDescriptions(
                'Красивый фахверковый особняк в историческом центре Дижона был построен в 1483 году. Это была резиденция богатого купца Гийома Мильера. В конце прошлого столетия дом Мильера был объявлен национальным памятником архитектуры, здесь появился сувенирный магазин, чайная комната и ресторан традиционной бургундской кухни.'
            );
        $restaurant2->setPicture('r2.jpg');
        $manager->persist($restaurant2);


        $restaurant3 = new Restaurants();
        $restaurant3
            ->setTitle('Л`Эпюзетт')
            ->setDescriptions(
                'Изысканный ресторан расположен на скалах очаровательного залива Валон дез Оф. Благодаря его стеклянным стенам создается ощущение, что это почти невесомое здание парит над землей. К тому же во время трапезы можно любоваться прекрасным видом на море. Фирменные блюда: тажин из омара с овощами; седло кролика в тесте; каннелони с клубникой и юзу.'
            );
        $restaurant3->setPicture('r3.jpg');
        $manager->persist($restaurant3);


        $restaurant4 = new Restaurants();
        $restaurant4
            ->setTitle('Сенекье')
            ->setDescriptions(
                'В кондитерской «Sénéquier» подают засахаренные каштаны из Коллобриера, круассаны с кедровыми орешками и провансальские консервированные фрукты, а также приготовленную по хранящемуся в тайне рецепту нугу из испанского миндаля, сицилийских фисташек и прованского меда. Знаменитый «тропецианский пирог» (легкий засахаренный бисквит с тремя видами крема) можно отведать на месте или взять с собой.'
            );
        $restaurant4->setPicture('r4.jpg');
        $manager->persist($restaurant4);


        $restaurant5 = new Restaurants();
        $restaurant5
            ->setTitle('Albert Premier')
            ->setDescriptions(
                'Это заведение на карте Шамони появилось фактически в начале XX века, когда бакалейщик Жозеф Каррье, видя бурное развитие железных дорог в регионе, решил построить скромный отель. Первая зимняя олимпиада, состоявшаяся в Шамони в 1924 году, привлекла сюда множество туристов. Постоянным гостем этих мест стал и Альберт I, король Бельгии и по совместительству альпинист.'
            );
        $restaurant5->setPicture('r5.jpg');
        $manager->persist($restaurant5);


        $restaurant6 = new Restaurants();
        $restaurant6
            ->setTitle('де Жан-Люк')
            ->setDescriptions(
                'Шеф-повар ресторана Жан-Люк с неудержимой фантазией день за днем изобретает здоровые блюда, которые находятся в гармонии с природой. В обеденном меню всего 13 блюд, а на ужин их предлагают 7, но каждый сможет здесь выбрать что-то себе по вкусу: черные макароны с печеным артишоком с грибами и салом; бриоши с ванилью, клубникой, малиной и с эстрагоновым шербетом; темпура из овсяного корня с тмином; овощной йогурт с желтым перцем.'
            );
        $restaurant6->setPicture('r6.jpg');
        $manager->persist($restaurant6);


        $dish1 = new Dishes();
        $dish1
            ->setName('Ароматный луковый суп')
            ->setDescriptions('Ароматный луковый суп — это гордость Франции. Практически все без исключения туристы стараются отведать это блюдо в каком-нибудь местном бистро. Впрочем, чтобы попробовать вкуснейший французский луковый суп по оригинальному рецепту, вовсе необязательно пересекать границу. Если вы терпеливы и внимательны, то сможете приготовить это блюдо на своей собственной кухне. ')
            ->setPrice('111')
            ->setRestaurants($restaurant1);
        $manager->persist($dish1);

        $dish2 = new Dishes();
        $dish2
            ->setName('Рататуй')
            ->setDescriptions('Много лет назад рататуй можно было увидеть только на столе у бедных крестьян, которые готовили его из всех ингредиентов, попадающихся под руку. Сегодня это блюдо подают в лучших ресторанах мира. По сути, это овощное рагу из сезонных овощей. Попробовать стоит обязательно.')
            ->setPrice('111')
            ->setRestaurants($restaurant1);
        $manager->persist($dish2);

        $dish3 = new Dishes();
        $dish3
            ->setName('Киш с грибами и шпинатом')
            ->setDescriptions('Киш — это популярное блюдо французской кухни. Оно представляет собой открытый пирог со всевозможными вкусными начинками. Приготовьте киш с грибами и шпинатом по нашему рецепту. Подавать это блюдо можно как в холодном, так и в горячем виде.')
            ->setPrice('111')
            ->setRestaurants($restaurant2);
        $manager->persist($dish3);

        $dish4 = new Dishes();
        $dish4
            ->setName('Суп "Буйабес"')
            ->setDescriptions('Во французской кухне при приготовлении изысканных блюд зачастую используются морепродукты. Например, в известном за пределами Франции супе "Буйабес". Это блюдо не только вкусное, но и полезное, ведь дары моря — это ценный источник белка. Подавайте суп с чесночным соусом. ')
            ->setPrice('111')
            ->setRestaurants($restaurant2);
        $manager->persist($dish4);

        $dish5 = new Dishes();
        $dish5
            ->setName(' Конфи из кролика')
            ->setDescriptions('Нежное и ароматное конфи из кролика со специями с легкостью можно повторить на своей кухне. Запаситесь самыми свежими и качественными ингредиентами и терпением. Результат вас определенно порадует.')
            ->setPrice('111')
            ->setRestaurants($restaurant3);
        $manager->persist($dish5);

        $dish6 = new Dishes();
        $dish6
            ->setName('Кордон блю')
            ->setDescriptions('Кордон блю наглядно доказывает, что Франция — это не только сыры, супы и вино, но и вкуснейшие мясные блюда. В своем классическом варианте это блюдо представляет собой шницель из телятины с начинкой из сыра и ветчины, обжаренный в панировке. Это даже звучит чертовски аппетитно!')
            ->setPrice('111')
            ->setRestaurants($restaurant3);
        $manager->persist($dish6);

        $dish7 = new Dishes();
        $dish7
            ->setName('Говядина по-бургундски')
            ->setDescriptions('Говядина по-бургундски, пожалуй, представляет собой самое насыщенное по вкусу и ароматное блюдо французской кухни. Освоить этот рецепт могут даже начинающие кулинары. Сочетание говядины с красным вином определенно стоит всех усилий и времени, проведенного на кухне.')
            ->setPrice('111')
            ->setRestaurants($restaurant4);
        $manager->persist($dish7);

        $dish8 = new Dishes();
        $dish8
            ->setName('Филе миньон')
            ->setDescriptions('Говяжий стейк можно приготовить по-разному. Во Франции филе миньон готовят даже с добавлением ревеня и пармской ветчины. Получается сытно, питательно, сочно и очень вкусно. Делимся интересным рецептом. ')
            ->setPrice('111')
            ->setRestaurants($restaurant4);
        $manager->persist($dish8);

        $dish9 = new Dishes();
        $dish9
            ->setName('Петух в вине')
            ->setDescriptions('Французы при приготовлении мяса или птицы зачастую используют в качестве маринада или соуса вино. Легендарное блюдо французской кухни — петух в вине — не является исключением. Собирайте гостей и готовьте это изысканное блюдо. Все останутся сытыми и довольными!')
            ->setPrice('111')
            ->setRestaurants($restaurant5);
        $manager->persist($dish9);

        $dish10 = new Dishes();
        $dish10
            ->setName('Рыбная сковорода по-марсельски')
            ->setDescriptions('На первый взгляд может показаться, что готовить это блюдо нелегко и бесконечно долго. На самом же деле все гораздо проще. Рыбную сковороду по-марсельски можно приготовить всего лишь за четверть часа. Вкусно, питательно и полезно!')
            ->setPrice('111')
            ->setRestaurants($restaurant5);
        $manager->persist($dish10);

        $dish11 = new Dishes();
        $dish11
            ->setName('Салат "Нисуаз"')
            ->setDescriptions('Впервые этот салат приготовили в Ницце. Именно этот факт, по-видимому, и предопределил вкусовые качества "Нисуаза" — он легкий, питательный, сытный и сочный. Все ингредиенты в этом блюде прекрасно сочетаются между собой. Попробуйте повторить салат "Нисуаз" в российских широтах. ')
            ->setPrice('111')
            ->setRestaurants($restaurant6);
        $manager->persist($dish11);

        $dish12 = new Dishes();
        $dish12
            ->setName('Куриный паштет')
            ->setDescriptions('Франция славится своими нежными и воздушными паштетами. Готовят паштет в Пятой республике в основном из телячьей или куриной печени и обязательно добавляют любимые специи по вкусу. Приготовьте для домашних куриный паштет. ')
            ->setPrice('111')
            ->setRestaurants($restaurant6);
        $manager->persist($dish12);


        $manager->flush();
    }

    /**
     * Sets the container.
     *
     * @param ContainerInterface|null $container A ContainerInterface instance or null
     */
    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }
}